import React, {Component} from 'react';
import { AsyncStorage } from 'react-native';
import { StackNavigator } from 'react-navigation';
import styles from './app/components/styles/style';
import Registerlogin from './app/components/user/Registerlogin';
import Profile from './app/components/user/Profile';



export const Application = StackNavigator(
  {
    Initialscreen: {screen: Registerlogin},
    Profile: {screen: Profile},
  },
  {
    navigationOptions: {
      header: null,
    }
  }
);


export default class App extends Component<Props> {

  constructor(props){
    super(props);
    this.getData = this.getData.bind(this);
  }

  componentDidMount = () => {
    this.getData();
  }

  getData = async () => {
    var userToken = await AsyncStorage.getItem('user-token');
    if(userToken.length){
      this.props.navigation.navigate('Profile');
    }
  }

  render() {
    return (
      <Application />
    );
  }
}
