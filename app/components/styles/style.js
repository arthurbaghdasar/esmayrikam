import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: '#2896d3',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2896d3',
        paddingLeft: 40,
        paddingRight: 40,
    },
    header: {
        fontSize: 24,
        marginBottom: 60,
        color: '#fff',
        fontWeight: 'bold',
    },
    textInput: {
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: '#fff',
    },
    input: {
        color: '#fff',
        backgroundColor: 'transparent',
    },
    btn: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#01c853',
        borderColor: "transparent",
        borderWidth: 0,
        borderRadius: 5,
        color: '#fff'
    },
    btnRegister: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#B71C1C',
        borderColor: "transparent",
        borderWidth: 0,
        borderRadius: 5,
        color: '#fff'
    },
    text: {
        color: '#fff',
        fontSize: 24,
    }
});

export default styles;