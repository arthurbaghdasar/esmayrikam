import React, {Component} from 'react';
import {
    View,
    KeyboardAvoidingView,
    TouchableOpacity,
    AsyncStorage,
    Alert
} from 'react-native';
import styles from '../styles/style';
import { TextField } from 'react-native-material-textfield';
import {Text, Button} from 'react-native-elements';
import configs from '../configs/config';
import base64 from 'base-64';

export default class Login extends Component<Props> {

    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }

    login = () => {

      fetch(configs.base_url + '/jwt/token',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json',
                'Authorization': 'Basic ' + base64.encode(this.state.username + ":" + this.state.password),
            },
        })
            .then((response) => {
                if(response.status == 200) {
                    response.json();
                } else {
                    response = false;
                    Alert.alert('Սխալ Մուտքանուն կամ գաղտնաբառ');
                } 
            })
            .then((res) => {
               
               if(res !== undefined && res.token){
                    AsyncStorage.setItem('user-token', res.token);
                    this.props.navigation.navigate('Profile');
                }
    
            })
            .done();



    };

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View >

                    <Text style={styles.header}>- Մուտք գործել -</Text>

                    <TextField
                        tintColor='#fff'
                        baseColor='#fff'
                        labelFontSize={16}           
                        style={styles.input}
                        label='Մուտքանուն'
                        ref={this.state.username}
                        onChangeText={ (username) => this.setState({username}) }
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='next'
                        underlineColorAndroid='transparent'
                    />

                    <TextField
                        tintColor='#fff'
                        baseColor='#fff'
                        labelFontSize={16}         
                        style={styles.input}
                        label='Գաղտնաբառ'
                        ref={this.state.password}
                        onChangeText={ (password) => this.setState({password}) }
                        underlineColorAndroid='transparent'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='go'
                        secureTextEntry={true}
                    />

                    <TouchableOpacity style={styles.btn} onPress={this.login}>
                        <Text style={{color: '#fff'}} h4>Մուտք</Text>
                    </TouchableOpacity>

                </View>
            </KeyboardAvoidingView>
        );
    }
}
