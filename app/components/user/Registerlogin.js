import React, {Component} from 'react';
import {
    Text,
    View
} from 'react-native'
import Signup from './Signup';
import Login from './Login';
import BottomNavigation, {
  ShiftingTab
} from 'react-native-material-bottom-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Registerlogin extends Component<Props> {
  constructor(props){
    super(props);
    this.state = {
        activeTab: 'login',
    }
}

componentDidMount = () => {
  this.props.navigation.navigate('Profile');
}
  tabs = [
    {
      key: 'login',
      icon: 'user',
      label: 'Մուտք',
      barColor: '#01c853',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'signup',
      icon: 'user-plus',
      label: 'Գրանցում',
      barColor: '#B71C1C',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    }
  ]

  renderIcon = icon => ({ isActive }) => (
    <Icon size={24} color="white" name={icon} />
  )

  renderTab = ({ tab, isActive }) => (
    <ShiftingTab
      isActive={isActive}
      key={tab.key}
      label={tab.label}
      renderIcon={this.renderIcon(tab.icon)}
    />
  )

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
         { (this.state.activeTab == 'login') ?  <Login /> : null }
         { (this.state.activeTab == 'signup') ?  <Signup /> : null }
        </View>
        <BottomNavigation
          onTabPress={newTab => this.setState({ activeTab: newTab.key })}
          renderTab={this.renderTab}
          tabs={this.tabs}
          useLayoutAnimation
        />
      </View>
    )
  }
}