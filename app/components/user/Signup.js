import React, {Component} from 'react';
import {
    View,
    KeyboardAvoidingView,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';
import { TextField } from 'react-native-material-textfield';
import {Text} from 'react-native-elements';
import styles from '../styles/style';
import configs from '../configs/config';
import base64 from 'base-64';
import Profile from './Profile';

import { StackNavigator } from 'react-navigation';




export const Application = StackNavigator(
  {
    Profile: {screen: Profile},
  },
  {
    navigationOptions: {
      header: null,
    }
  }
);

export default class Signup extends Component<Props> {

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
        }
    
        this.Application = Application;
        this.signup = this.signup.bind(this);
    }


    signup = async () => {
        console.log(this);
        this.navigation.navigate('Profile');
       try {
            let response = await fetch(configs.base_url + '/user/register?_format=hal_json',{
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/hal+json',
                },
                body: JSON.stringify({
                    "_links": {
                        "type": {
                            "href": configs.base_url + "/rest/type/user/user"
                        }
                    },
                    "name":[{
                        "value": this.state.email
                    }],
                    "mail":[{
                        "value":this.state.email
                    }],
                    "pass":[{
                        "value": this.state.password
                    }],
                })
            });
            let responseJson = await response.json();
            
           if(response.status >= 200 && response.status < 300 ) {
            //Check if user was created and uid was generated
            if(responseJson.uid !== undefined && responseJson.uid[0].value > 1){
                try{
                    let authresponse = await fetch(configs.base_url + '/jwt/token',{
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-type': 'application/json',
                            'Authorization': 'Basic ' + base64.encode(this.state.email + ":" + this.state.password),
                        },
                    });
                    let authresponseJson = await authresponse.json();
                    console.log(authresponseJson.token);
                    if(authresponseJson.token.length){
                        AsyncStorage.setItem('user-token', authresponseJson.token);
                        this.navigation.navigate('Profile');
                    }
            
                } 
                catch (e){
                    console.log(e)
                }
            }
        } else{
            let error = responseJson;
            throw error;
        }
          } catch (error) {
            console.error(error);
        }
      
    };

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View >

                    <Text style={styles.header}>- Գրանցվել -</Text>

                   
                   <TextField
                        tintColor='#fff'
                        baseColor='#fff'
                        labelFontSize={16}               
                        style={styles.input}
                        label='Էլ-հասցե'
                        ref={this.state.email}
                        onChangeText={ (email) => this.setState({email}) }
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='next'
                        underlineColorAndroid='transparent'
                    />

                   
                   <TextField
                        tintColor='#fff'
                        baseColor='#fff'
                        labelFontSize={16}               
                        style={styles.input}
                        label='Գաղտնաբառ'
                        ref={this.state.password}
                        onChangeText={ (password) => this.setState({password}) }
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='go'
                        underlineColorAndroid='transparent'
                        secureTextEntry={true}
                    />


                    <TouchableOpacity style={styles.btnRegister} onPress={() => this.signup()}>
                        <Text  style={{color: '#fff'}} h4>Գրանցվել</Text>
                    </TouchableOpacity>

                </View>
            </KeyboardAvoidingView>
        );
    }
}
